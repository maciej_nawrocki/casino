using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Website.Models;
using Website.Services;

namespace Website.Controllers
{
    public class ControllerUserAware: Controller
    {
        private UserManager<UserModel> _userManager;
        
        public Task<UserModel> CurrentUser {
            get {
                return GetCurrentUser();
            }
        }

        public ControllerUserAware(UserManager<UserModel> userManager) {
            _userManager = userManager;
        }

        private async Task<UserModel> GetCurrentUser() {
            return await _userManager.GetUserAsync(this.User);
        }
    }
}