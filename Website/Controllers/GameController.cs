using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Website.Models;
using Website.Services;

namespace Website.Controllers
{
    public class GameController : ControllerUserAware
    {

        private readonly IGamesService gamesService;
        public GameController(UserManager<UserModel> userManager, IGamesService gamesService): base(userManager) {
            this.gamesService = gamesService;
        }

        public async Task<IActionResult> Index()
        {
            var user = await CurrentUser;
            if (user == null) {
                return Unauthorized();
            }
            var game = await gamesService.GetGameOfUser(user.Id);
            return View();
        }
    
        public async Task<IActionResult> StartGame(Guid roomId)
        {
            await gamesService.CreateGameAsync(roomId, 0);

            return RedirectToAction("Index");
        }
    }
}