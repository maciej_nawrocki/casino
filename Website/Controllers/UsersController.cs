using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Website.Models;
using Website.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Website.Data;

namespace Website.Controllers {
    public class UsersController: Controller {

        public UsersController(UserManager<UserModel> um, IUserService usersService) {
            _usersService = usersService;
            userManager = um;
            //userManager = new UserManager<IdentityUser>(new UserStore(context));
        }
        private readonly IUserService _usersService;
        private UserManager<UserModel> userManager;

        public IActionResult Index() {
            var model = new UserListModel() {
                Users = userManager.Users.ToList()
            };
           
            return View(model);
        }

    }
}