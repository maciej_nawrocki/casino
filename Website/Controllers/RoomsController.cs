using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Website.Models;
using Website.Services;

namespace Website.Controllers
{
    public class RoomsController: ControllerUserAware
    {
        private readonly IRoomsService roomsService;
        public RoomsController(UserManager<UserModel> userManager, IRoomsService roomsService): base(userManager) {
            this.roomsService = roomsService;
        }

        public async Task<IActionResult> Index() {
            var rooms = await roomsService.getAllRoomsAsync();

            var user = await CurrentUser;
            if (user != null) {
                rooms.CurrentUser = user;
            }
            
            return View(rooms);
        }

        public async Task<IActionResult> Create() {
            var user = await CurrentUser;
            if (user == null) {
                return Unauthorized();
            }
            await roomsService.CreateRoom(user.Id);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Join(String roomToJoinId) {
            var user = await CurrentUser;
            if (user == null) {
                return Unauthorized();
            }
            await roomsService.AddUserToRoom(user.Id, roomToJoinId);

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Leave(String roomToLeaveId) {
            var user = await CurrentUser;
            if (user == null) {
                return Unauthorized();
            }
            
            await roomsService.RemoveUserFromRoom(user.Id, roomToLeaveId);

            return RedirectToAction("Index");
        }
    }
}