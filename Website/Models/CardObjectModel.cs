using System;

namespace Website.Models {
    public class CardObjectModel {
        private static Random rand = new Random();

        public CardObjectModel(int symbol, int position)
        {
            this.symbol = symbol;
            this.position = position;
            this.rotation = rand.Next(24) * 15;
        }

        public Guid Id { get; set; }
        public int symbol { get; set; }
        public int position { get; set; } 
        public int rotation { get; set; } // (0-345, every 15deg)
    }
}