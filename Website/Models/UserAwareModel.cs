using System;
using System.Collections.Generic;

namespace Website.Models {
    public class UserAwareModel {
        public UserModel CurrentUser { get; set; }
    }
}