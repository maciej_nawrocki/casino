using System;

namespace Website.Models {
        public class PointsModel {
        public Guid Id { get; set; }
        public UInt32 points { get; set; }
        public UserModel player { get; set; }
    }
}