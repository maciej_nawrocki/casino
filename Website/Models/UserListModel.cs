using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Website.Models {
    public class UserListModel {
        public List<UserModel> Users {get; set;}
    }
}