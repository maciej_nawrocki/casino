using System;

namespace Website.Models {
    
    public class GameResultsModel {
        public Guid Id { get; set; }
        public DateTime gameTime { get; set; }
        public PointsModel[] points { get; set; }
    }

}