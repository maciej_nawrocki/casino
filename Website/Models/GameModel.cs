using System;
using System.Collections.Generic;

namespace Website.Models {
    public class GameModel {
        public Guid Id { get; set; }
        public int Order { get; set; }
        public RoomModel Room { get; set; }
        public ICollection<CardModel> Cards { get; set; }
        public int CurrentCard { get; set; } = 0;
    }
}