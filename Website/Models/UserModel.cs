using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Website.Models {
    public class UserModel: IdentityUser {
        public DateTime registerDate { get; set; }
        public DateTime lastOnline { get; set; }
        public RoomModel Room { get; set; }
    }
}