using System;
using System.Collections.Generic;

namespace Website.Models {
    public class CardModel {
        public Guid Id { get; set; }
        public int Order { get; set; }
        public ICollection<CardObjectModel> Objects { get; set; } = new LinkedList<CardObjectModel>();
        public string OwnerId { get; set; }
        public UserModel Owner { get; set; } = null;
    }
}