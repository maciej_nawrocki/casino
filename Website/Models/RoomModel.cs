using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Website.Models {
    public class RoomModel {
        const int PLAYERS_LIMIT = 5;
        public Guid Id { get; set; }
        public ICollection<UserModel> Users { get; set; }
        public ICollection<GameModel> Games { get; set; }
        public int limit = PLAYERS_LIMIT;

        public RoomModel() {
            Users = new List<UserModel>();
            Games = new List<GameModel>();
        }
    }
}