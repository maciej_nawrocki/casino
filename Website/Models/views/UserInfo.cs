namespace Website.Models.views
{
    public class UserInfo
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int CollectedCards { get; set; } = 0;
    }
}