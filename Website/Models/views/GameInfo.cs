using System;
using System.Collections.Generic;

namespace Website.Models.views
{
    public class GameInfo
    {
        public Guid Id { get; set; }
        public int Round { get; set; }
        public List<UserInfo> Users { get; set; } = new List<UserInfo>();
        public string PlayerId { get; set; }
    }
}