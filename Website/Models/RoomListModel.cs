using System.Collections.Generic;

namespace Website.Models
{
    public class RoomListModel : UserAwareModel
    {
        public List<RoomModel> Rooms { get; set; }
    }
}