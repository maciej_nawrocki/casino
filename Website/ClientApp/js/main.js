import '@babel/polyfill';
import '../styles/style.less';

import $ from 'jquery';
global.jQuery = global.$ = $;

import 'popper.js';
import 'bootstrap';
import 'jquery-validation';
import 'jquery-validation-unobtrusive';

import * as signalR from '@aspnet/signalr';
import * as msgpack from '@aspnet/signalr-protocol-msgpack';
import Game from './game';

const connection = new signalR.HubConnectionBuilder()
        .withUrl("/game-data")
        .configureLogging(signalR.LogLevel.Trace)
        .withHubProtocol(new msgpack.MessagePackHubProtocol())
        .build();
        
const game = new Game(connection);

connection.start().then(() => {
        game.requestGameInfo();
});

