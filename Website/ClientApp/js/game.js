export default class Game {
    constructor(connection) {
        this.connection = connection;
        this.round = 0;
        this.playerId = null;
        this.player = null;
        this.otherPlayers = [];

        this.onGameInfo = this.onGameInfo.bind(this);
        this.onGetNextCard = this.onGetNextCard.bind(this);
        this.onGetPlayerCard = this.onGetPlayerCard.bind(this);
        this.onEndOfGame = this.onEndOfGame.bind(this);
        this.onEndOfRound = this.onEndOfRound.bind(this);
        this.onCardTaken = this.onCardTaken.bind(this);
        this.onTakeCard = this.onTakeCard.bind(this);
        this.printPlayerCard = this.printPlayerCard.bind(this);
        this.printStackCard = this.printStackCard.bind(this);

        this.handleResponse("GetGameInfo", this.onGameInfo);
        this.handleResponse("GetNextCard", this.onGetNextCard);
        this.handleResponse("GetPlayerCard", this.onGetPlayerCard);
        this.handleResponse("TakeCard", this.onTakeCard);
        this.handleResponse("EndOfRound", this.onEndOfRound);
        this.handleResponse("EndOfGame", this.onEndOfGame);
        this.handleResponse("CardTaken", this.onCardTaken);
    }

    async requestGameInfo() {
        await this.connection.invoke("GetGameInfo");
    }

    onGameInfo(gameInfo) {
        console.log("gameInfo", gameInfo);
        this.otherPlayers = [];
        this.playerId = gameInfo.PlayerId;
        this.round = gameInfo.Round;

        gameInfo.Users.forEach(user => {
            if (user.Id === this.playerId) {
                this.player = user;
            } else {
                this.otherPlayers.push(user);
            }
        });

        this.updatePlayer();
        this.updateRound();
        this.updateEnemies();

        this.requestPlayerCard();
        this.requestNextCard();
    }

    onEndOfGame() {
        console.log("endOfGame");

        // TODO: Jakiś ekran wynikowy gry
    }

    onEndOfRound() {
        console.log("endOfRound");
        var results = [];
        for(let i = 0; i < this.otherPlayers.length; i++) {
            results.push(this.otherPlayers[i].Name + " " +  this.otherPlayers[i].CollectedCards);
        }
        results.push(this.player.Name + " " +  this.player.CollectedCards);
        $("#rjkegbuaectdyofttnrectg").html(results.join(" <br/>").toString());
    }

    onCardTaken(userId) {
        console.log("cardTaken", userId);
        const user = this.otherPlayers.find(u => u.Id = userId);
        if (user) {
            user.CollectedCards++;
        }

        this.updateEnemies();

        // TODO: Animacja przesuwania karty na stos przeciwnika
    }

    requestNextCard() {
        this.connection.invoke("GetNextCard");
    }

    onGetNextCard(card) {
        console.log("stack", card);
        this.printStackCard(card.Objects);
    }

    requestPlayerCard() {
        this.connection.invoke("GetPlayerCard");
    }

    onGetPlayerCard(card) {
        console.log("player", card);
        this.printPlayerCard(card);
    }

    requestTakeCard(cardId, symbol) {
        console.log("take", cardId, symbol);
        this.connection.invoke("TakeCard", cardId, symbol);
    }

    onTakeCard(successful) {
        console.log("take card", successful);

        if (successful) {
            this.player.CollectedCards++;
        }

        this.updatePlayer();
    }

    printStackCard(card) {
        $('#stack-card').empty();
        card.forEach(element => {
            $('<div/>', {
                class: `object pos-${element.position + 1} tile-${element.symbol + 1} rotate-${element.rotation}`
            }).appendTo('#stack-card');
        });
    }

    printPlayerCard(card) {
        const that = this;
        $('#user-card').empty();
        card.Objects.forEach(element => {
            $('<div/>', {
                class: `object pos-${element.position + 1} tile-${element.symbol + 1} rotate-${element.rotation}`
            })
                .appendTo('#user-card')
                .on('click', event => {
                    event.preventDefault();
                    that.requestTakeCard(card.Id, element.symbol);
                });
        });
    }

    updatePlayer() {
        $('.user-board .username').text(`${this.player.Name} (${this.player.CollectedCards})`);
    }

    updateEnemies() {
        for(let i = 0; i < 5; i++) {
            this.updateEnemy(this.otherPlayers.length <= i ? null : this.otherPlayers[i], i);
        }
    }

    updateEnemy(user, index) {
        const elementId = `.opponent-${index + 1}`;
        if (!user) {
            $(elementId).hide(0);
        } else {
            $(elementId).show(0);

            $(`${elementId} .username`).text(`${user.Name} (${user.CollectedCards})`);
        }
    }

    updateRound() {
        $("#round").text(this.round);
    }

    handleResponse(method, callback) {
        this.connection.on(method, callback);
    }
}