#DB: MySQL
- Docker

1. docker pull mysql
2. docker run -p 3306:3306 --name mysqlserver -e MYSQL_ROOT_PASSWORD=password -d mysql:latest
3. docker exec -it mysqlserver bash
4. mysql -u root -p -e "create database dobble"; 
5. password

# Init db w folderze Website:
dotnet ef database update

# Instalacja
1. w folderze Website/ClientApp: npm install

# Uruchamianie
1. Uruchomić DB MySQL
2. w folderze Website: dotnet watch run lub Debug w IDE

# Deploy na heroku
https://blog.devcenter.co/deploy-asp-net-core-2-0-apps-on-heroku-eea8efd918b6
nie ma budowania `npm install` w pipeline, dlatego trzeba przed commitem zbudować ręcznie