﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Website.Models;

namespace Website.Data
{
    public class ApplicationDbContext : IdentityDbContext<UserModel>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<GameResultsModel> GameResults { get; set; }
        public DbSet<PointsModel> Points { get; set; }
        public DbSet<CardObjectModel> CardObjects { get; set; }
        public DbSet<CardModel> Cards { get; set; }
        public DbSet<RoomModel> Rooms { get; set; }
        public DbSet<GameModel> Games { get; set; }

        protected override void OnModelCreating(ModelBuilder builder) {
            base.OnModelCreating(builder);
        }
    }
}
