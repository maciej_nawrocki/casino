using Website.Data;

namespace Website.Services
{
    public class ServiceContextAware
    {
        protected readonly ApplicationDbContext _context;
        public ServiceContextAware(ApplicationDbContext context)
        {
            _context = context;
        }
    }
}