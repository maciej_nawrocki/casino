using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Website.Data;
using Website.Models;

namespace Website.Services
{
    public class RoomsService : ServiceContextAware, IRoomsService
    {
        public RoomsService(ApplicationDbContext context) : base(context) { }

        public async Task<bool> CreateRoom(String userId)
        {
            var creator = await FindUserWithRoomById(userId);
            if (creator.Room != null)
            {
                throw new Exception("User is already assigned to room");
            }

            RoomModel room = new RoomModel();
            room.Id = Guid.NewGuid();
            room.Users.Add(creator);
            creator.Room = room;

            _context.Rooms.Add(room);

            var saveResult = await _context.SaveChangesAsync();
            return saveResult == 2; // room and user
        }

        public async Task<RoomListModel> getAllRoomsAsync()
        {
            var list = new RoomListModel();
            list.Rooms = await _context.Rooms.Include(r => r.Users).Include(r => r.Games).ToListAsync();

            return list;
        }
        
        public async Task<bool> AddUserToRoom(String userToAddId, String roomId)
        {
            var foundUser = await FindUserWithRoomById(userToAddId);
            if (foundUser.Room != null)
            {
                throw new Exception("User is already assigned to some room");
            }

            var foundRoom = await FindRoomWithUsersById(roomId);
            if (foundRoom == null)
            {
                throw new Exception("Invalid rooom");
            }

            if (foundRoom.Users.Count == foundRoom.limit)
            {
                throw new Exception("User cannot join to this room, because maximum number of players is reached there");
            }

            foundRoom.Users.Add(foundUser);
            foundUser.Room = foundRoom;

            var result = await _context.SaveChangesAsync();
            return true;
        }
        public async Task<bool> RemoveUserFromRoom(String userToRemoveId, String roomId)
        {
            var foundUser = await FindUserWithRoomById(userToRemoveId);
            if (foundUser.Room == null)
            {
                return true;
            }
            var foundRoom = await FindRoomWithUsersById(roomId);
            if (foundRoom == null)
            {
                throw new Exception("Invalid rooom");
            }
            if (foundRoom.Users.Contains(foundUser))
            {
                foundRoom.Users.Remove(foundUser);

                if (foundRoom.Users.Count == 0)
                {
                    _context.Rooms.Remove(foundRoom);
                }
            }
            foundUser.Room = null;

            var saveResult = await _context.SaveChangesAsync();
            return true;
        }
        public async Task<RoomModel> FindRoomWithUsersById(String roomId)
        {
            return await _context.Rooms.Include(r => r.Users).FirstOrDefaultAsync(room => room.Id.Equals(new Guid(roomId)));
        }
        public async Task<UserModel> FindUserWithRoomById(String userId)
        {
            return await _context.Users.Include(r => r.Room).FirstOrDefaultAsync(u => u.Id == userId);
        }
        public async Task<RoomModel> FindRoomWithGamesByUser(String userId)
        {
            return await _context.Rooms
                .Include(r => r.Users)
                .Include(r => r.Games)
                    .ThenInclude(g => g.Cards)
                        .ThenInclude(c => c.Objects)
                
                .FirstOrDefaultAsync(room => room.Users.Any(u => u.Id == userId));
        }
    }
}