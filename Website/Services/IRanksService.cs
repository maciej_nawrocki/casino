using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Website.Models;
using Microsoft.AspNetCore.Identity;

namespace Website.Services {
    public interface IRanksService {
        Task<GameResultsModel[]> getFinishedGamesAsync();
        Task<PointsModel[]> getPointsForGameAsync(Guid game);
    }
}