using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Website.Models;
using Microsoft.AspNetCore.Identity;
using Website.Data;

namespace Website.Services {
    public class SomeRanksService: IRanksService {
        public Task<GameResultsModel[]> getFinishedGamesAsync() {
            return Task.FromResult(new GameResultsModel[] {});
        }

        public Task<PointsModel[]> getPointsForGameAsync(Guid game) {
            return Task.FromResult(new PointsModel[] {});
        }
    }
}