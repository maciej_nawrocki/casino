using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Website.Models;
using Microsoft.AspNetCore.Identity;

namespace Website.Services {
    public interface IGamesService {

        Task CreateGameAsync(Guid roomId, int order);
        Task<GameModel> GetGameOfUser(string userId);
        Task SetCardOwner(CardModel currentCard, UserModel player);
        Task SetNextCard(GameModel gameModel);
    }
}