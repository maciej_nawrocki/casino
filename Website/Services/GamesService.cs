using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Website.Models;
using Microsoft.AspNetCore.Identity;
using Website.Data;
using Website.Utils;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Website.Services {
    public class GamesService : ServiceContextAware, IGamesService
    {
        private IRoomsService roomsService;
        private static Dobbler dobbler = new Dobbler();
        public GamesService(ApplicationDbContext context, IRoomsService roomsService) : base(context) {
            this.roomsService = roomsService;
        }
        public async Task CreateGameAsync(Guid roomId, int order) {
            var foundRoom = await FindRoomById(roomId);
            if (foundRoom == null) {
                return;
            }
            // if (foundRoom.Users.Contains(currentUser)) throw error

            var game = new GameModel { Order = order, Cards = createDeck() };

            var cards = game.Cards.ToArray().OrderBy(c => c.Order).ToList();
            var i = 0;
            foreach(var user in foundRoom.Users) {
                cards[i++].Owner = user;
            }
            game.CurrentCard = i;

            foundRoom.Games.Add(game);
            _context.Games.Add(game);

            await _context.SaveChangesAsync();
        }
        private List<CardModel> createDeck() {
            var list = new List<CardModel>();
            List<List<int>> stack = null;
            bool stackFine = false;

            while (!stackFine) {
                stack = dobbler.CreateDobbleDeck(5);
                stackFine = dobbler.CheckDobbleDeck(stack);
            }

            Utils.Utils.Shuffle(stack);
            int i = 0;
            foreach(List<int> symbols in stack) {
                list.Add(createCard(i++, symbols));
            }

            return list;
        }
        private CardModel createCard(int order, List<int> symbols) {
            var card = new CardModel { Order = order };
            int position = 0;
            Utils.Utils.Shuffle(symbols);
            foreach(int symbol in symbols) {
                card.Objects.Add(new CardObjectModel(symbol, position++));
            }

            return card;
        }
        public async Task<RoomModel> FindRoomById(Guid roomId)
        {
            return await _context.Rooms.Include(r => r.Users).FirstOrDefaultAsync(u => u.Id == roomId);
        }
        public async Task<GameModel> GetGameOfUser(string id)
        {
            var room = await roomsService.FindRoomWithGamesByUser(id);
            if (room == null || room.Games.Count == 0) {
                return null;
            }
            return room.Games.ToArray()[room.Games.Count - 1];
        }
        public async Task SetCardOwner(CardModel currentCard, UserModel player)
        {
            currentCard.Owner = player;

            await _context.SaveChangesAsync();
        }
        public async Task SetNextCard(GameModel gameModel)
        {
            gameModel.CurrentCard++;

            await _context.SaveChangesAsync();
        }
    }
}