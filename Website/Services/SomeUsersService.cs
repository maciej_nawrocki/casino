using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Website.Models;
using Microsoft.AspNetCore.Identity;
using Website.Data;
using System.Linq;

namespace Website.Services {
    public class SomeUsersService: IUserService {
        public Task<UserModel[]> GetIncompleteUsersAsync() {

            /* var context = new ApplicationDbContext(options =>
        options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            var allUsers = context.Users.ToList(); */

            // var user1 = new UserModel
            // {
            //     registerDate = new DateTime(2017, 07, 11), //Habilitacja daciana
            //     lastOnline = new DateTime(2018, 06, 12),
            //     idUser = null,
            //     Id = Guid.Empty
            // };

            return Task.FromResult(new UserModel[] {});
        }
    }
}