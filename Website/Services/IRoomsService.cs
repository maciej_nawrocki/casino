using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Website.Models;
using Microsoft.AspNetCore.Identity;

namespace Website.Services {
    public interface IRoomsService {
        Task<RoomListModel> getAllRoomsAsync();
        Task<bool> CreateRoom(String userId);
        Task<bool> AddUserToRoom(String userToAddId, String roomId);
        Task<bool> RemoveUserFromRoom(String userToRemoveId, String roomId);
        Task<RoomModel> FindRoomWithUsersById(String roomId);
        Task<RoomModel> FindRoomWithGamesByUser(String userId);
        Task<UserModel> FindUserWithRoomById(String userId);
    }
}