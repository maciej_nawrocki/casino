using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR;
using Website.Utils;
using Website.Services;
using Website.Data;
using Microsoft.AspNetCore.Identity;
using Website.Models;
using System.Linq;
using Website.Models.views;
using System;

namespace Website.Hubs 
{
    public class GameHub : Hub {

        private IGamesService gamesService;
        private UserManager<UserModel> userManager;
        public GameHub(UserManager<UserModel> userManager, IGamesService gamesService) : base() {
            this.userManager = userManager;
            this.gamesService = gamesService;
        }
        public async Task GetGameInfo() {
            var player = await GetCurrentUser();
            var game = await GetCurrentGame();

            var gameInfo = new GameInfo { Id = game.Id, Round = game.Order + 1, PlayerId = player.Id };

            foreach(UserModel user in game.Room.Users) {
                var collectedCards = game.Cards.Where(c => c.OwnerId != null && c.OwnerId == user.Id).Count();
                gameInfo.Users.Add(new UserInfo { Id = user.Id, Name = user.UserName, CollectedCards = collectedCards });
            }

            await Clients.Caller.SendAsync("GetGameInfo", gameInfo);
        }
        public async Task GetPlayerCard() {
            var card = await GetPlayerCurrentCard();
            var wrapped = new {
                Id = card.Id,
                Objects = card.Objects
            };
            await Clients.Caller.SendAsync("GetPlayerCard", wrapped);
        }
        public async Task TakeCard(Guid cardId, int symbol) {
            var player = await GetCurrentUser();
            var game = await GetCurrentGame();
            var playerCard = await GetPlayerCurrentCard();
            var currentCard = await GetCurrentStackCard();

            var successful = currentCard.Objects.Where(o => o.symbol == symbol).Count() == 1 
                           && playerCard.Objects.Where(o => o.symbol == symbol).Count() == 1;

            if (successful) {
                await gamesService.SetCardOwner(currentCard, player);
                await gamesService.SetNextCard(await GetCurrentGame());
                currentCard = await GetCurrentStackCard();
            }

            var endOfGame = currentCard == null;

            if (successful) {
                await Clients.Others.SendAsync("CardTaken", player.Id);

                if (!endOfGame) {
                    await Clients.All.SendAsync("GetNextCard", currentCard); // send info about new card at stack to all
                    await GetPlayerCard(); // send new card to player
                } else {
                    if (game.Order == 5) { // 5 rounds
                        await Clients.All.SendAsync("EndOfGame");
                    } else {
                        await Clients.All.SendAsync("EndOfRound");
                        await gamesService.CreateGameAsync(game.Room.Id, game.Order + 1);
                        await Task.Delay(2000); // delay to next round
                        await GetGameInfo(); // send info about new game
                    }
                }
            }

            await Clients.Caller.SendAsync("TakeCard", successful); // send info about taken card to all
        }
        public async Task GetNextCard() {
            var card = await GetCurrentStackCard();
            var wrapped = new {
                Id = card.Id,
                Objects = card.Objects
            };
            await Clients.Caller.SendAsync("GetNextCard", wrapped);
        }

        private async Task<UserModel> GetCurrentUser() {
            return await userManager.GetUserAsync(Context.User);
        }
        private async Task<CardModel> GetPlayerCurrentCard() {
            var player = await GetCurrentUser();
            var game = await GetCurrentGame();

            var cards = game.Cards.ToArray().OrderBy(c => c.Order).ToList();
            var playerCard = cards.Where(c => c.OwnerId != null && c.OwnerId == player.Id).Reverse().FirstOrDefault();
            return playerCard;
        }
        private async Task<CardModel> GetCurrentStackCard() {
            var game = await GetCurrentGame();
            var cards = game.Cards.ToArray().OrderBy(c => c.Order).ToList();

            if (cards.Count() <= game.CurrentCard) {
                return null;
            }
            return cards[game.CurrentCard];
        }

        private async Task<GameModel> GetCurrentGame() {
            var user = await GetCurrentUser();
            if (user == null) {
                return null;
            }
            return await gamesService.GetGameOfUser(user.Id);
        }
    }
}